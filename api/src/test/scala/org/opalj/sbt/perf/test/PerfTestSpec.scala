/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.sbt.perf.test

import java.io.File

import org.opalj.sbt.perf.MeasurementExecutor
import org.scalatest._

import scala.collection.JavaConverters

/**
 * Abstract Spec for performance tests. Provides a measurementExecutor instance for the tests.
 * After the test suit finishes, the performance measurements are deleted, in between test cases,
 * they are preserved.
 *
 * @author Andreas Muttscheller
 */
abstract class PerfTestSpec extends FlatSpec with Matchers with BeforeAndAfterAll {
    val testClasspath = Thread.currentThread.getContextClassLoader.getResource("")
    var measurementExecutor = MeasurementExecutor(
        JavaConverters.seqAsJavaList(Seq(new File(testClasspath.getFile)))
    )

    override def afterAll(): Unit = {
        measurementExecutor.cleanMeasurements()
    }
}

trait ClearMeasurementsAfterEach extends PerfTestSpec with BeforeAndAfterEach {
    override def afterEach(): Unit = {
        measurementExecutor.cleanMeasurements()
    }
}

trait NewMeasurementExecutorInstanceAfterEach extends PerfTestSpec with BeforeAndAfterEach {
    override def afterEach(): Unit = {
        measurementExecutor = MeasurementExecutor(
            JavaConverters.seqAsJavaList(Seq(new File(testClasspath.getFile)))
        )
    }
}
