/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.sbt.perf.test

import org.opalj.sbt.perf.exceptions.{  DuplicateMeasurementNameException,
                                        MeasurementExceedsErrorThresholdException}
import org.opalj.sbt.perf.test.perf.{DuplicateMeasurementSpec, Sleep}

/**
 * Test for the most basic functionality
 *
 * @author Andreas Muttscheller
 */
class SingleMeasureTest extends PerfTestSpec {

    "sbt-perf" should "be able to measure Sleep with 100 ms" in {
        Sleep.time = 100
        assert(measurementExecutor.measureOnly(classOf[Sleep].getCanonicalName))
        assert(Sleep.executed)
    }

    it should "give a warning when the warning threshold will be exceeded" in {
        Sleep.time = 130
        assert(!measurementExecutor.measureOnly(classOf[Sleep].getCanonicalName))
    }

    it should "throw an exception when error threshold will be exceeded" in {
        Sleep.time = 160
        assertThrows[MeasurementExceedsErrorThresholdException] {
            !measurementExecutor.measureOnly(classOf[Sleep].getCanonicalName)
        }
    }

    it should "throw an exception if two measurements have the same name" in {
        DuplicateMeasurementSpec.setDuplcate
        assertThrows[DuplicateMeasurementNameException] {
            measurementExecutor.measureOnly(classOf[DuplicateMeasurementSpec].getCanonicalName)
        }
        DuplicateMeasurementSpec.setDifferent
    }

    it should "be ok if the runtime is shorter" in {
        Sleep.time = 50
        assert(measurementExecutor.measureOnly(classOf[Sleep].getCanonicalName))
    }
}
