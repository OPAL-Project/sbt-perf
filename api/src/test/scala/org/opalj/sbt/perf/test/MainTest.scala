/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.sbt.perf.test

import org.opalj.sbt.perf.test.perf.{AssertionPerfFalse, MemoryUsage, Sleep}
import org.opalj.sbt.perf.{Main, MeasurementExecutor}
import org.scalatest.PrivateMethodTester

import scala.collection.mutable.ListBuffer
import scala.reflect.io.File

/**
 * Test the main method, it should pass all options properly into the MeasurementExecutor.
 *
 * @author Andreas Muttscheller
 */
class MainTest extends PerfTestSpec with PrivateMethodTester with ClearMeasurementsAfterEach {

    private def setMeasurementOptions(
        runtimeThresholdWarning: Double,
        runtimeThresholdError: Double,
        memoryThresholdWarning: Double,
        memoryThresholdError: Double,
        runRuntimeMeasurements: Boolean,
        runMemoryMeasurements: Boolean,
        updateMeasurements: Boolean
    ): Seq[String] = {
        var arguments = ListBuffer(
            "--runtimeThresholdWarning", runtimeThresholdWarning.toString,
            "--runtimeThresholdError", runtimeThresholdError.toString,
            "--memoryThresholdWarning", memoryThresholdWarning.toString,
            "--memoryThresholdError", memoryThresholdError.toString,
            "--classpath", testClasspath.toURI.getPath
        )

        if (!runRuntimeMeasurements) {
            arguments += "--disableRuntimeMeasurements"
        }
        if (!runMemoryMeasurements) {
            arguments += "--disableMemoryMeasurements"
        }
        if (updateMeasurements) {
            arguments += "--updateMeasurements"
        }

        arguments
    }

    "sbt-perf-main" should "pass all options to the MeasurementExecutor instance" in {
        val classNameSleep = classOf[Sleep].getCanonicalName
        val classNameMemory = classOf[MemoryUsage].getCanonicalName
        val args = setMeasurementOptions(
            1, // runtime warning
            2, // runtime error
            3, // memory warning
            4, // memory error
            false, // run runtime measurements
            false, // run memory measurements
            true // update measurements
        ) ++ Seq(
            classNameSleep
        )

        val m = Main.runMeasurementExecutor(args.toArray)
        assert(m.defaultRuntimeThresholdWarning == 1)
        assert(m.defaultRuntimeThresholdError == 2)
        assert(m.defaultMemoryThresholdWarning == 3)
        assert(m.defaultMemoryThresholdError == 4)
        assert(m.runRuntimeMeasurements == false)
        assert(m.runMemoryMeasurements == false)
        assert(m.updateMeasurements == true)

        val loadPreviousMeasurements =
            PrivateMethod[MeasurementExecutor.MeasurementMap]('loadPreviousMeasurements)
        val previousResults = measurementExecutor invokePrivate loadPreviousMeasurements()
        assert(previousResults.contains(classNameSleep))
        assert(!previousResults.contains(classNameMemory))
    }

    it should "be able to run all measurements" in {
        AssertionPerfFalse.failTest = false
        val classNameSleep = classOf[Sleep].getCanonicalName
        val classNameMemory = classOf[MemoryUsage].getCanonicalName
        val args = Seq(
            "--classpath", testClasspath.toURI.getPath
        )
        Main.main(args.toArray)

        val loadPreviousMeasurements =
            PrivateMethod[MeasurementExecutor.MeasurementMap]('loadPreviousMeasurements)
        val previousResults = measurementExecutor invokePrivate loadPreviousMeasurements()
        assert(previousResults.contains(classNameSleep))
        assert(previousResults.contains(classNameMemory))
    }

    it should "be able to run only one measurement" in {
        val classNameSleep = classOf[Sleep].getCanonicalName
        val classNameMemory = classOf[MemoryUsage].getCanonicalName
        val args = Seq(
            "--classpath", testClasspath.toURI.getPath,
             classNameSleep
        )
        Main.main(args.toArray)

        val loadPreviousMeasurements =
            PrivateMethod[MeasurementExecutor.MeasurementMap]('loadPreviousMeasurements)
        val previousResults = measurementExecutor invokePrivate loadPreviousMeasurements()
        assert(previousResults.contains(classNameSleep))
        assert(!previousResults.contains(classNameMemory))
    }

    it should "be able to clear one measurement" in {
        val classNameSleep = classOf[Sleep].getCanonicalName
        val classNameMemory = classOf[MemoryUsage].getCanonicalName
        assert(measurementExecutor.measureOnly(classNameSleep))
        assert(measurementExecutor.measureOnly(classNameMemory))
        assert(File(".perf").exists)

        val args = Seq(
            "--cleanOnly", classNameSleep
        )
        Main.main(args.toArray)
        assert(File(".perf").exists)

        val loadPreviousMeasurements =
            PrivateMethod[MeasurementExecutor.MeasurementMap]('loadPreviousMeasurements)
        val previousResults = measurementExecutor invokePrivate loadPreviousMeasurements()
        assert(!previousResults.contains(classNameSleep))
        assert(previousResults.contains(classNameMemory))
    }

    it should "be able to clear all measurements" in {
        val classNameSleep = classOf[Sleep].getCanonicalName
        val classNameMemory = classOf[MemoryUsage].getCanonicalName
        assert(measurementExecutor.measureOnly(classNameSleep))
        assert(measurementExecutor.measureOnly(classNameMemory))
        assert(File(".perf").exists)

        val args = Seq(
            "--clean"
        )
        Main.main(args.toArray)
        assert(!File(".perf").exists)
    }
}
