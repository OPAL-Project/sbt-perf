/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.sbt.perf.test

import org.opalj.sbt.perf.MeasurementExecutor
import org.opalj.sbt.perf.test.perf.{AbstractPerfSpecImpl, MeasurementInAbstractPerfSpec, Sleep}
import org.scalatest.PrivateMethodTester

import scala.reflect.io.File

/**
 * Test for the most basic functionality
 *
 * @author Andreas Muttscheller
 */
class ClearMeasurementTest extends PerfTestSpec
    with ClearMeasurementsAfterEach with PrivateMethodTester {

    "sbt-perf-clear" should "be able to remove the .perf file" in {
        Sleep.time = 100
        assert(measurementExecutor.measureOnly(classOf[Sleep].getCanonicalName))
        assert(File(".perf").exists)
        measurementExecutor.cleanMeasurements()
        assert(!File(".perf").exists)
    }

    it should "not create a new .perf file when it doesn't exist" in {
        assert(!File(".perf").exists)
        measurementExecutor.cleanMeasurements()
        assert(!File(".perf").exists)
    }

    it should "not create a new file if .perf doesn't exist and a single measurement gets " +
        "deleted" in {

        assert(!File(".perf").exists)
        measurementExecutor.cleanMeasurement(classOf[Sleep].getCanonicalName)
        assert(!File(".perf").exists)
    }

    it should "be able to remove a single measurement and remove the file when its the last" in {
        Sleep.time = 100
        assert(measurementExecutor.measureOnly(classOf[Sleep].getCanonicalName))

        assert(File(".perf").exists)
        measurementExecutor.cleanMeasurement(classOf[Sleep].getCanonicalName)
        assert(!File(".perf").exists)
    }

    it should "remove the measurement from the file, but not remove the complete file for more " +
        "measurements" in {
        Sleep.time = 100
        val classNameSleep = classOf[Sleep].getCanonicalName
        assert(measurementExecutor.measureOnly(classNameSleep))

        val className = classOf[AbstractPerfSpecImpl].getCanonicalName
        assert(measurementExecutor.measureOnly(className))
        assert(MeasurementInAbstractPerfSpec.abstractExecuted)
        assert(File(".perf").exists)

        val loadPreviousMeasurements =
            PrivateMethod[MeasurementExecutor.MeasurementMap]('loadPreviousMeasurements)
        val previousResults = measurementExecutor invokePrivate loadPreviousMeasurements()

        assert(previousResults.contains(className))
        assert(previousResults.contains(classNameSleep))

        measurementExecutor.cleanMeasurement(className)
        assert(File(".perf").exists)

        val currentResults = measurementExecutor invokePrivate loadPreviousMeasurements()
        assert(!currentResults.contains(className))
        assert(currentResults.contains(classNameSleep))

    }
}
