/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.sbt.perf.test.perf

import org.opalj.sbt.perf.MeasurementResult
import org.opalj.sbt.perf.spec.{AssertResults, PerfSpec}

class AssertionPerfTrue extends PerfSpec with AssertResults {
    measure("1MB array", measureRuntime = false) {
        new Array[Byte](1024*1024)
    }

    measure("2MB array", measureRuntime = false) {
        new Array[Byte](2*1024*1024)
    }

    override def assertResults(measurements: Map[String, MeasurementResult]) = {
        // 1MB + small overhead for array object
        assert(measurements("1MB array").memoryUsage < 1024*1024 + 100)
        assert(measurements("1MB array").memoryUsage < measurements("2MB array").memoryUsage)
    }
}

class AssertionPerfFalse extends PerfSpec with AssertResults {
    measure("1MB array", measureRuntime = false) {
        new Array[Byte](1024*1024)
    }

    measure("2MB array", measureRuntime = false) {
        new Array[Byte](2*1024*1024)
    }

    override def assertResults(measurements: Map[String, MeasurementResult]) = {
        if (AssertionPerfFalse.failTest)
            assert(
                measurements("1MB array").memoryUsage > measurements("2MB array").memoryUsage,
                "1MB array should be smaller than the 2MB array"
            )
    }
}

object AssertionPerfFalse {
    var failTest = true
}
