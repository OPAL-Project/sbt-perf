/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.sbt.perf

import java.io.File
import java.io.FileNotFoundException
import java.io.PrintWriter
import java.lang.reflect.Modifier
import java.net.URLClassLoader
import java.util.logging.Level
import java.util.logging.Logger

import org.opalj.br.BaseConfig
import org.opalj.br.ObjectType
import org.opalj.br.analyses.Project
import org.opalj.io.process
import org.opalj.io.processSource
import org.opalj.log.ConsoleOPALLogger
import org.opalj.log.GlobalLogContext
import org.opalj.log.LogContext
import org.opalj.log.OPALLogger
import org.opalj.log.{Error => ErrorLogLevel}
import org.opalj.sbt.perf.exceptions.AssertionErrorException
import org.opalj.sbt.perf.exceptions.MeasurementExceedsErrorThresholdException
import org.opalj.sbt.perf.spec.PerfSpec
import play.api.libs.json.Json

import scala.collection.JavaConverters
import scala.io.Source.fromFile

/**
 * Class to execute measurements.
 *
 * Parameters need to be java types because it is called from a class loader that is executed in a
 * scala 2.10 context because of sbt. This code is run in scala 2.11. Since the versions are not
 * bytecode compatible, the parameters must be passed as java objects.
 *
 * @param projectCpList A list of classpaths to look for [[spec.PerfSpec]] derived classes.
 * @author Andreas Muttscheller
 */
class MeasurementExecutor(projectCpList: java.util.List[File]) {
    type MeasurementMap = MeasurementExecutor.MeasurementMap

    var logger = Logger.getGlobal()
    System.setProperty("java.util.logging.SimpleFormatter.format", "%5$s%n");
    logger.setLevel(Level.INFO)

    var defaultRuntimeThresholdError = MeasurementExecutor.defaultRuntimeThresholdError
    var defaultRuntimeThresholdWarning = MeasurementExecutor.defaultRuntimeThresholdWarning
    var defaultMemoryThresholdError = MeasurementExecutor.defaultMemoryThresholdError
    var defaultMemoryThresholdWarning = MeasurementExecutor.defaultMemoryThresholdWarning

    /**
     * Measure the runtime of all measurements to be executed
     */
    var runRuntimeMeasurements = true

    /**
     * Measure the memory consumption of all measurements to be executed
     */
    var runMemoryMeasurements = true

    /**
     * Update measurements in .perf file if a measurement file already exist
     */
    var updateMeasurements = false

    /**
     * List containing the classpath where the [[PerfSpec]] implemented classes are.
     */
    private val perfClassesClasspath = JavaConverters.asScalaBuffer(projectCpList)

    /**
     * Classloader containing the classpath to the [[PerfSpec]] measurements.
     * It will be used to load and instanciate the [[PerfSpec]] derived classes.
     */
    private val perfClassLoader = new URLClassLoader(
        perfClassesClasspath.map(_.toURI.toURL).toArray,
        this.getClass.getClassLoader
    )

    /**
     * Checks if the difference between the previous runtime and the current one exceeds the
     * warning threshold and is smaller than the error threshold.
     *
     * @param r A runtime comparison object containing the previous and current runtimes.
     * @return true if the difference exceeds the thresholdWarning and is smaller than the
     *         errorThreshold, false if the differences is smaller than the threshold or larger
     *         than the errorThreshold.
     */
    def runtimeExceedsWarningThreshold(r: PerfComparison): Boolean =
        r.percentDiff > r.currentMeasurement.runtimeWarningThreshold.t &&
            r.percentDiff <= r.currentMeasurement.runtimeErrorThreshold.t

    /**
     * Checks if the difference between the previous runtime and the current one exceeds the
     * error threshold.
     *
     * @param r A runtime comparison object containing the previous and current runtimes.
     * @return true if the difference exceeds the thresholdError, false if the differences is
     *         smaller than the threshold.
     */
    def runtimeExceedsErrorThreshold(r: PerfComparison): Boolean =
        r.percentDiff > r.currentMeasurement.runtimeErrorThreshold.t

    /**
     * Checks if the difference between the previous memory usage and the current one exceeds the
     * warning threshold.
     *
     * @param r A runtime comparison object containing the previous and current memory usage.
     * @return true if the difference exceeds the thresholdWarning, false if the differences is
     *         smaller than the threshold.
     */
    def memoryExceedsWarningThreshold(r: PerfComparison): Boolean =
        r.memoryDiff > r.currentMeasurement.memoryWarningThreshold.t &&
            r.memoryDiff <= r.currentMeasurement.memoryErrorThreshold.t

    /**
     * Checks if the difference between the previous memory usage and the current one exceeds the
     * error threshold.
     *
     * @param r A runtime comparison object containing the previous and current memory usage.
     * @return true if the difference exceeds the thresholdError, false if the differences is
     *         smaller than the threshold.
     */
    def memoryExceedsErrorThreshold(r: PerfComparison): Boolean =
        r.memoryDiff > r.currentMeasurement.memoryErrorThreshold.t

    /**
     * Measure all classes found in the classpath provided by the contructor parameter.
     *
     * @return true if the measurement were executed successfully without any errors or warning
     *         and the results are saved in the file specified in [[MeasurementExecutor.PerfFile]].
     *         false is returned if one or more warnings were issued. In this case, the
     *         measurements aren't saved.
     */
    def measureAll(): Boolean = {
        measure(getPerfTestClassNames())
    }

    /**
     * Measure one class.
     *
     * @param measureClass The name of the class to be measured.
     * @return true if the measurement were executed successfully without any errors or warning
     *         and the results are saved in the file specified in [[MeasurementExecutor.PerfFile]].
     *         false is returned if one or more warnings were issued. In this case, the
     *         measurements aren't saved.
     */
    def measureOnly(measureClass: String): Boolean = {
        val allPerfTestClasses = getPerfTestClassNames()

        if (allPerfTestClasses.exists(c => c.equals(measureClass))) {
            measure(List(measureClass))
        } else {
            logger.log(Level.SEVERE, s"Class $measureClass not found.")
            throw new NoClassDefFoundError(s"Class $measureClass not found.")
        }
    }

    /**
     * Run the performance measurements for [[PerfSpec]] derived classes in the given classpaths.
     *
     * @param perfTests                   A list of class names to be measured.
     * @return true if the measurement were executed successfully without any errors or warning
     *         and the results are saved in the file specified in [[MeasurementExecutor.PerfFile]].
     *         false is returned if one or more warnings were issued. In this case, the
     *         measurements aren't saved.
     * @throws MeasurementExceedsErrorThresholdException If one or more measurements are exceeding
     *                                                   the error threshold, an Exception is
     *                                                   thrown.
     */
    private def measure(
        perfTests: List[String]
    ): Boolean = {
        // Load previous results
        val prevResults = loadPreviousMeasurements()

        // Load all classes that derive from PerfSpec and create runners for them.
        val perfSpecRunners = perfTests
            .map { name => (name, loadClass(name)) }
            .filter { case (name, perfClass) => isClassInstantiable(perfClass) }
            .map { case (name, perfClass) =>
                (name, PerfSpecRunner(
                    perfClass,
                    runRuntimeMeasurements,
                    runMemoryMeasurements
                )) }

        // Run all measurements
        perfSpecRunners.foreach { case (name, perfSpecRunner) =>
            perfSpecRunner.measurePerfSpec()
        }

        // Get the results
        val currentResults = perfSpecRunners.map { x =>
            (x._1, x._2.measurementResults)
        }.toMap

        // Check runtimes and print log messages
        val measurementDiffs = getMeasurementsDiff(currentResults, prevResults)

        // Print log messages in case of threshold exceedance
        measurementDiffs
            .foreach { r =>
                logger.log(Level.INFO, s"${Utils.ANSI_GREEN}Results for ${r.measurementName} ${Utils.ANSI_RESET}")

                // Runtime comparison
                if (r.currentMeasurement.avg() > 0) {
                    val avgCurrent = r.currentMeasurement.avg() / 1000000000.0
                    val avgStored = r.previousMeasurement.avg() / 1000000000.0
                    logger.log(Level.INFO,
                        s"${Utils.ANSI_GREEN}" +
                            f"- Averag runtime is $avgCurrent%1.3f s, " +
                            f"was $avgStored%1.3f s " +
                            f"${if (r.percentDiff >= 0) "+" else ""}${r.percentDiff * 100}%1.2f %%" +
                            s"${Utils.ANSI_RESET}"
                    )
                }
                if (runtimeExceedsWarningThreshold(r)) {
                    logger.log(Level.WARNING, f"${Utils.ANSI_YELLOW}- ${r.measurementName} " +
                        f"runtime threshold exceeded by ${r.percentDiff * 100}%1.2f %%!${Utils.ANSI_RESET}")
                } else if (runtimeExceedsErrorThreshold(r)) {
                    logger.log(Level.SEVERE, f"${Utils.ANSI_RED}- ${r.measurementName} " +
                        f"runtime threshold exceeded by ${r.percentDiff * 100}%1.2f %%!${Utils.ANSI_RESET}")
                }
                if (r.previousMeasurement.avg() > 0) {
                    val avgSeconds = r.previousMeasurement.avg() / 1000000000.0
                    logger.log(Level.INFO,
                        f"${Utils.ANSI_CYAN}- Stored runtime is $avgSeconds%1.3f s. ${Utils.ANSI_RESET}"
                    )
                }

                // Memory comparison
                if (r.currentMeasurement.memoryUsage > 0) {
                    logger.log(Level.INFO,
                        s"${Utils.ANSI_GREEN}" +
                            s"- Measurement uses ${r.currentMeasurement.memoryUsage} byte " +
                            s"(${Utils.asMB(r.currentMeasurement.memoryUsage)}), " +
                            s"was ${r.previousMeasurement.memoryUsage} byte " +
                            s"(${Utils.asMB(r.previousMeasurement.memoryUsage)}). " +
                            f"${if (r.memoryDiff >= 0) "+" else ""}${r.memoryDiff * 100}%1.2f %%" +
                            s"${Utils.ANSI_RESET}"
                    )
                }
                if (memoryExceedsWarningThreshold(r)) {
                    logger.log(Level.WARNING, f"${Utils.ANSI_YELLOW}- ${r.measurementName} " +
                        f"memory usage threshold exceeded by ${r.memoryDiff * 100}%1.2f %%!${Utils.ANSI_RESET}")
                } else if (memoryExceedsErrorThreshold(r)) {
                    logger.log(Level.SEVERE, f"${Utils.ANSI_RED}- ${r.measurementName} " +
                        f"memory usage threshold exceeded by ${r.memoryDiff * 100}%1.2f %%!${Utils.ANSI_RESET}")
                }
            }

        // Do we have assertion errors?
        if (perfSpecRunners.exists(_._2.hasAssertionErrors)) {
            throw AssertionErrorException(perfSpecRunners.count(_._2.hasAssertionErrors))
        }

        if (measurementDiffs.exists(runtimeExceedsErrorThreshold) ||
            measurementDiffs.exists(memoryExceedsErrorThreshold)) {
            // Throw an exception if some error occurred
            throw MeasurementExceedsErrorThresholdException(
                measurementDiffs.filter(runtimeExceedsErrorThreshold) union
                measurementDiffs.filter(memoryExceedsErrorThreshold)
            )
        } else if (!measurementDiffs.exists(runtimeExceedsWarningThreshold) &&
                    !measurementDiffs.exists(memoryExceedsWarningThreshold)) {
            // We have a warning, only save measurements if desired or new
            if (updateMeasurements || prevResults.isEmpty) {
                saveMeasurements(currentResults)
            } else {
                val measurementsToSave =
                    (currentResults -- prevResults.keySet) ++ // Add new perf classes
                    prevResults.map { case (className, prevMeasurements) =>
                        // Add previous results and new measurements in existing classes
                        var measurements = prevMeasurements
                        if (currentResults.contains(className)) {
                            val newMeasurements = currentResults(className) filterNot { m =>
                                prevMeasurements.exists(p => m.measurementName == p.measurementName)
                            }
                            measurements ++= newMeasurements
                        }
                        (className, measurements)
                    }
                saveMeasurements(measurementsToSave)
            }
        }

        // return true if no warnings / errors occurred, false otherwise
        !measurementDiffs.exists(runtimeExceedsWarningThreshold) &&
        !measurementDiffs.exists(memoryExceedsWarningThreshold)
    }

    /**
     * Delete all previous performance measurements.
     *
     * @note This removes the file specified in [[MeasurementExecutor.PerfFile]] from the hard disk.
     */
    def cleanMeasurements(): Unit = {
        val f = new File(MeasurementExecutor.PerfFile)
        if (f.exists()) {
            if (f.delete()) {
                logger.log(Level.INFO, s"Performance measurements ${f.getAbsolutePath} removed.")
            } else {
                logger.log(Level.SEVERE, s"Error deleting file ${f.getAbsolutePath}.")
            }
        } else {
            logger.log(Level.FINE, s"No performance measurements found.")
        }
    }

    /**
     * Remove the measurement for the given class from the file specified in
     * [[MeasurementExecutor.PerfFile]]. All other measurements will be saved.
     *
     * @note The [[MeasurementExecutor.PerfFile]] will be removed if no more measurements are
     *       present.
     * @param perfClass The class name of the measurement to remove.
     */
    def cleanMeasurement(perfClass: String): Unit = {
        val measurements = loadPreviousMeasurements()
        if (measurements.nonEmpty) {
            if (measurements.contains(perfClass)) {
                saveMeasurements(measurements - perfClass)
                logger.log(Level.FINE, s"Removed measurement for class $perfClass successfully.")
                if (loadPreviousMeasurements().isEmpty) {
                    logger.log(Level.FINE, s"No more measurements in file. Deleting it.")
                    cleanMeasurements()
                }
            } else {
                logger.log(Level.FINE, s"No measurement of class $perfClass found. Do nothing.")
            }
        }
    }

    /**
     * Get a list of classes that derive from [[PerfSpec]]. Loads the classpath
     * into a OPAL project to get the classes.
     *
     * @return A list of classes that derive from [[PerfSpec]].
     */
    private def getPerfTestClassNames(): List[String] = {
        val logContext: LogContext = GlobalLogContext
        OPALLogger.updateLogger(GlobalLogContext, new ConsoleOPALLogger(true, ErrorLogLevel))
        val p = Project(
            perfClassesClasspath.toArray,
            Array.empty[File],
            logContext,
            BaseConfig
        )
        p
            .classHierarchy
            .allSubtypes(ObjectType(MeasurementExecutor.PerfSpecOt), reflexive = false)
            .filter { ot =>
                val cf = p.classFile(ot).get
                !cf.isAbstract && !cf.isInterfaceDeclaration && cf.hasDefaultConstructor
            }
            .map(ot ⇒ ot.toJava)
            .toList
    }

    /**
     * Load previous measurements from the file specified in [[MeasurementExecutor.PerfFile]].
     *
     * @return A map containing the class name as key and another map with the measurement name
     *         and the average runtime.
     */
    private def loadPreviousMeasurements(): MeasurementMap = {
        // Load previous results from file
        try {
            processSource(fromFile(MeasurementExecutor.PerfFile)) { r =>
                val jsonString = r.getLines.mkString
                Json.parse(jsonString).as[MeasurementMap]
            }
        } catch {
            case _: FileNotFoundException ⇒ {
                logger.log(Level.FINE, "No previous measurements found!")
                Map.empty[String, List[MeasurementResult]]
            }
        }
    }

    /**
     * Save the measurement results of the current run to the file specified in
     * [[MeasurementExecutor.PerfFile]].
     *
     * @param measurements A Map containing the measurement results.
     */
    private def saveMeasurements(measurements: MeasurementMap) = {
        // TODO: Save measurements for each git commit?
        process(new PrintWriter(MeasurementExecutor.PerfFile)) { r =>
            val f = new File(MeasurementExecutor.PerfFile).getAbsolutePath
            val json = Json.prettyPrint(Json.toJson(measurements))
            r.write(json)
            logger.log(Level.FINE, s"Measurements written to $f")
        }
    }

    /**
     * Load [[PerfSpec]] class.
     *
     * @param className The name of the [[PerfSpec]] derived class to load.
     * @return A Class[PerfSpec] object representing the given className.
     */
    private def loadClass(className: String): Class[PerfSpec] = {
        logger.log(Level.FINE, s"Loading class $className")

        val p = perfClassLoader.loadClass(className)
        p.asInstanceOf[Class[PerfSpec]]
    }

    /**
     * Check if class is instantiable.
     *
     * @param perfClass The class to check.
     * @return true if the class is abstract and not instantiable, false if not and defines
     *         and accessible constructor.
     */
    private def isClassInstantiable(perfClass: Class[PerfSpec]): Boolean = {
        !Modifier.isAbstract(perfClass.getModifiers) &&
            perfClass.getConstructors.exists(_.getParameterCount == 0)
    }

    /**
     * Consumes two maps and flattens the nested map into a list of [[MeasurementResult]].
     *
     * @param currentResults  A map containing the current measurement results.
     * @param previousResults A map containing the previous measurement times.
     * @return A list of [[MeasurementResult]].
     */
    private def getMeasurementsDiff(
        currentResults: MeasurementMap,
        previousResults: MeasurementMap
    ): List[PerfComparison] = {
        // Get only the classes that are measured in this run
        (currentResults filterKeys previousResults.keySet)
            // flatten the resulting tuples to a list
            .flatMap { case (className, measurementMap) =>
            // Discard old measurements
            measurementMap.filter(m => previousResults(className).exists(m2 => m2
                .measurementName == m.measurementName))
                // Build the return tulpe
                .map { measurement =>
                    val previousTime = previousResults(className).find(
                        m => m.measurementName == measurement.measurementName
                    ).head

                    PerfComparison(
                        className,
                        measurement.measurementName,
                        measurement,
                        previousTime
                    )
                }
            }
            .toList
    }
}

/**
 * Define factory methods and constants for [[MeasurementExecutor]].
 *
 * @author Andreas Muttscheller
 */
object MeasurementExecutor {
    type MeasurementMap = Map[String, Iterable[MeasurementResult]]

    final val PerfSpecOt = "org/opalj/sbt/perf/spec/PerfSpec"
    final val PerfFile = ".perf"

    /**
     * If a measurement exceeds the [[defaultRuntimeThresholdError]] percentage compared to the
     * previous run, an error exception is thrown and the measurement fails.
     */
    var defaultRuntimeThresholdError = 0.5

    /**
     * If a measurement exceeds the [[defaultRuntimeThresholdWarning]], a message is printed on
     * the console. The result won't be saved until the measurement has a lower
     * runtime and doesn't exceed the threshold anymore.
     */
    var defaultRuntimeThresholdWarning = 0.25

    /**
     * If a measurement exceeds the [[defaultMemoryThresholdError]] percentage compared to the
     * previous run, an error exception is thrown and the measurement fails.
     */
    var defaultMemoryThresholdError = 0.5

    /**
     * If a measurement exceeds the [[defaultMemoryThresholdWarning]], a message is printed on
     * the console. The result won't be saved until the measurement has a lower
     * memory usage and doesn't exceed the threshold anymore.
     */
    var defaultMemoryThresholdWarning = 0.25

    def apply(projectCpList: java.util.List[File]): MeasurementExecutor =
        new MeasurementExecutor(projectCpList)

    def apply(projectCpList: List[File]): MeasurementExecutor = {
        new MeasurementExecutor(JavaConverters.seqAsJavaList(projectCpList))
    }
}
