/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.sbt.perf

import java.io.File
import java.net.URLClassLoader

import scala.annotation.tailrec

/**
 * Main object for PerfSpec.
 *
 * @author Andreas Muttscheller
 */
object Main {
    def main(args: Array[String]): Unit = {
        runMeasurementExecutor(args)
    }

    /**
     * Parse the command line arguments and create a MeasurementExecutor instance. Run the
     * desired function afterwards.
     *
     * @note The split is needed to test the MeasurementExecutor instance, if all passed
     *       command parameters are properly set in the object.
     *
     * @param args Command line arguments, as passed to `main`.
     * @return The MeasurmementExecutor instance that was run.
     */
    def runMeasurementExecutor(args: Array[String]): MeasurementExecutor = {
        type OptionMap = Map[Symbol, Any]
        val thisClasspath = List(
            new File(Thread.currentThread.getContextClassLoader.getResource("").getFile)
        )

        @tailrec
        def nextArg(map: OptionMap, args: List[String]): OptionMap = {
            args match {
                case Nil => map
                case "--classpath" :: cp :: tail =>
                    val cpFile = cp.split(",").map(c => new File(c)).toList
                    nextArg(map + ('classpath -> cpFile), tail)
                case "--disableMemoryMeasurements" :: tail =>
                    nextArg(map + ('runMemoryMeasurements -> false), tail)
                case "--disableRuntimeMeasurements" :: tail =>
                    nextArg(map + ('runRuntimeMeasurements -> false), tail)
                case "--updateMeasurements" :: tail =>
                    nextArg(map + ('updateMeasurements -> true), tail)
                case "--runtimeThresholdWarning" :: value :: tail =>
                    nextArg(map + ('runtimeThresholdWarning -> value.toDouble), tail)
                case "--runtimeThresholdError" :: value :: tail =>
                    nextArg(map + ('runtimeThresholdError -> value.toDouble), tail)
                case "--memoryThresholdWarning" :: value :: tail =>
                    nextArg(map + ('memoryThresholdWarning -> value.toDouble), tail)
                case "--memoryThresholdError" :: value :: tail =>
                    nextArg(map + ('memoryThresholdError -> value.toDouble), tail)
                case "--clean" :: tail =>
                    nextArg(map + ('action -> "clean"), tail)
                case "--cleanOnly" :: className :: tail =>
                    nextArg(map + ('action -> "cleanOnly", 'className -> className), tail)
                case className :: Nil =>
                    map + ('action -> "measureOnly", 'className -> className)
                case option :: tail =>
                    println(s"Unknown option $option")
                    System.exit(1)
                    map
            }
        }

        val argMap = nextArg(Map(), args.toList)

        // Create new MeasurementExecutor instance depending on classpath parameter
        var m = MeasurementExecutor(thisClasspath)
        if (argMap.contains('classpath)) {
            m = MeasurementExecutor(argMap('classpath).asInstanceOf[List[File]])
        }

        // Set switches
        m.runRuntimeMeasurements =
            argMap.getOrElse('runRuntimeMeasurements, m.runRuntimeMeasurements)
                .asInstanceOf[Boolean]
        m.runMemoryMeasurements =
            argMap.getOrElse('runMemoryMeasurements, m.runMemoryMeasurements)
                .asInstanceOf[Boolean]
        m.updateMeasurements =
            argMap.getOrElse('updateMeasurements, m.updateMeasurements)
                .asInstanceOf[Boolean]

        // Set thresholds
        if (argMap.contains('runtimeThresholdWarning))
            m.defaultRuntimeThresholdWarning = argMap('runtimeThresholdWarning).asInstanceOf[Double]
        if (argMap.contains('runtimeThresholdError))
            m.defaultRuntimeThresholdError = argMap('runtimeThresholdError).asInstanceOf[Double]
        if (argMap.contains('memoryThresholdWarning))
            m.defaultMemoryThresholdWarning = argMap('memoryThresholdWarning).asInstanceOf[Double]
        if (argMap.contains('memoryThresholdError))
            m.defaultMemoryThresholdError = argMap('memoryThresholdError).asInstanceOf[Double]

        argMap.getOrElse('action, "measureAll") match {
            case "clean" => m.cleanMeasurements()
            case "cleanOnly" => m.cleanMeasurement(argMap('className).asInstanceOf[String])
            case "measureAll" => m.measureAll()
            case "measureOnly" => m.measureOnly(argMap('className).asInstanceOf[String])
        }

        m
    }
}
