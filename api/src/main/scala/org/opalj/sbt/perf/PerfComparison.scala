/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.sbt.perf

/**
 * Class that contains information about the current and previous
 * runtime of a single measurement. If the measurement is executed the first time,
 * previousRuntime is 0.
 *
 * @param className       The class where the measurement is implemented in.
 * @param measurementName The name of the measurement.
 * @param currentMeasurement  The current runtime in ns.
 * @param previousMeasurement The runtime of the previous execution in ns.
 * @author Andreas Muttscheller
 */
case class PerfComparison(
    className: String,
    measurementName: String,
    currentMeasurement: MeasurementResult,
    previousMeasurement: MeasurementResult
) {
    /**
     * Calculate the percentage diff between the average previousRuntime and the average current
     * runtime.
     *
     * @return The relative performance difference between the previous measurement and the
     *         current one.
     */
    def percentDiff: Double = {
        if (previousMeasurement.avg() == 0) {
            return 0
        }
        val diff = currentMeasurement.avg() - previousMeasurement.avg()
        diff / previousMeasurement.avg().toDouble
    }

    /**
     * Calculate the percentage diff between memory usage of two runs.
     *
     * @return The relative memory usage difference between the previous run and the current one.
     *         If the previous or current run has no memory usage, 0 is returned.
     */
    def memoryDiff: Double = {
        if (previousMeasurement.memoryUsage == 0 || currentMeasurement.memoryUsage == 0) {
            return 0
        }

        val diff = currentMeasurement.memoryUsage - previousMeasurement.memoryUsage
        diff / previousMeasurement.memoryUsage.toDouble
    }
}

