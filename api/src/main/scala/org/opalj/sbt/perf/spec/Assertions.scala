/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.sbt.perf.spec

/**
 * Trait to add assert statement in measurement definitions. It adds the code put into `assert` into
 * the falseAssertions list which will later be read by the MeasurementExecutor.
 *
 * @author Andreas Muttscheller
 */
trait Assertions {
    import scala.language.experimental.macros

    def assert(cond: Boolean): Unit = macro AssertionsMacro.assertImpl
    def assert(cond: Boolean, clue: String): Unit = macro AssertionsMacro.assertClueImpl
}

/**
 * Macro for assertion.
 *
 * @author Andreas Muttscheller
 */
object AssertionsMacro {
    import scala.reflect.macros.blackbox

    def assertImpl(
        c: blackbox.Context
    )(
        cond: c.Expr[Boolean]
    ): c.Expr[Unit] = {
        import c.universe._
        c.Expr(
            q"""
                if (!${cond.tree}) {
                    throw new org.opalj.sbt.perf.exceptions.AssertionError(
                        ${showCode(cond.tree)}
                    )
                }
             """)
    }

    def assertClueImpl(
        c: blackbox.Context
    )(
        cond: c.Expr[Boolean],
        clue: c.Expr[String]
    ): c.Expr[Unit] = {
        import c.universe._
        c.Expr(
            q"""
                if (!${cond.tree}) {
                    throw new org.opalj.sbt.perf.exceptions.AssertionError(
                        ${showCode(cond.tree)},
                        ${clue.tree.toString}
                    )
                }
             """)
    }
}
