/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.sbt.perf

import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

/**
 * Class containing the execution time of each measurement run of a measurement.
 *
 * @param measurementName The name of the measurement
 * @param consideredExecutionTimes The runtimes considered for the measurements.
 * @param memoryUsage The amount of bytes the computed object graph uses or -1 if memory
 *       measurement was disabled.
 * @param runtimeWarningThreshold A threshold when to write a warning on runtime measurements.
 * @param runtimeErrorThreshold A threshold when to throw an exception on runtime measurements.
 * @param memoryWarningThreshold A threshold when to write a warning on memory measurements.
 * @param memoryErrorThreshold A threshold when to throw an exception on memory measurements.
 *
 * @author Andreas Muttscheller
 */
case class MeasurementResult(
    measurementName: String,
    consideredExecutionTimes: Seq[Long],
    memoryUsage: Long,
    runtimeWarningThreshold: RuntimeWarningThreshold,
    runtimeErrorThreshold: RuntimeErrorThreshold,
    memoryWarningThreshold: MemoryWarningThreshold,
    memoryErrorThreshold: MemoryErrorThreshold
) {
    /**
     * The the average execution time of this measurement
     *
     * @return The average execution time of the measurement in Nanoseconds
     */
    def avg(): Long = {
        if (consideredExecutionTimes.nonEmpty) {
            consideredExecutionTimes.sum / consideredExecutionTimes.size
        } else {
            0
        }
    }
}

/**
 * Companion object for MeasurementResult.
 *
 * @author Andreas Muttscheller
 */
object MeasurementResult {
    implicit val measurementResultsWriter = new Writes[MeasurementResult] {
        def writes(measurementResult: MeasurementResult) = Json.obj(
            "measurement" -> measurementResult.measurementName,
            "consideredExecutionTimes" → measurementResult.consideredExecutionTimes,
            "memoryUsage" -> measurementResult.memoryUsage,
            "runtimeWarningThreshold" -> measurementResult.runtimeWarningThreshold,
            "runtimeErrorThreshold" -> measurementResult.runtimeErrorThreshold,
            "memoryWarningThreshold" -> measurementResult.memoryWarningThreshold,
            "memoryErrorThreshold" -> measurementResult.memoryErrorThreshold
        )
    }

    implicit val measurementResultsReads: Reads[MeasurementResult] = (
        (JsPath \ "measurement").read[String] and
            (JsPath \ "consideredExecutionTimes").read[Seq[Long]] and
            (JsPath \ "memoryUsage").read[Long] and
            (JsPath \ "runtimeWarningThreshold").read[RuntimeWarningThreshold] and
            (JsPath \ "runtimeErrorThreshold").read[RuntimeErrorThreshold] and
            (JsPath \ "memoryWarningThreshold").read[MemoryWarningThreshold] and
            (JsPath \ "memoryErrorThreshold").read[MemoryErrorThreshold]
        ) (MeasurementResult.apply _)

}