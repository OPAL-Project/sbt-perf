/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.sbt.perf

import java.lang.management.ManagementFactory
import java.lang.reflect.InvocationTargetException
import java.util.logging.Level
import java.util.logging.Logger

import org.github.jamm.MemoryMeter
import org.opalj.sbt.perf.exceptions.AssertionError
import org.opalj.sbt.perf.spec.AssertResults
import org.opalj.sbt.perf.spec.Measurement
import org.opalj.sbt.perf.spec.PerfSpec
import org.opalj.util.PerformanceEvaluation.time
import org.opalj.util.gc

/**
 * Class to run all measurements inside a PerfSpec class
 *
 * @param perfSpecClass The class to measure. It must derive from PerfSpec.
 * @param runRuntimeMeasurements If true, runtime measurements are executed.
 * @param runMemoryMeasurements If true, memory consumption is measured.
 *
 * @author Andreas Muttscheller
 */
class PerfSpecRunner(
    perfSpecClass: Class[PerfSpec],
    runRuntimeMeasurements: Boolean = true,
    runMemoryMeasurements: Boolean = true
) {
    var logger = Logger.getGlobal()

    var measurementResults = Iterable.empty[MeasurementResult]

    var hasAssertionErrors = false

    /**
     * Time the performance measurements and memory consumption of a given class. Afterwards,
     * execute the assertResults method if the `AssertResults` trait was mixed in.
     */
    def measurePerfSpec(): Unit = {
        gc() // org.opalj.util.gc

        try {
            val perfSpecInstance = perfSpecClass.getConstructor().newInstance()

            val measurements = perfSpecInstance.measureTests

            measurementResults = measurements.map(runMeasurement)

            // Run result assertions
            perfSpecInstance match {
                case assertResultsInstance: AssertResults =>
                    val resultMap = measurementResults.map(r => r.measurementName -> r).toMap
                    try {
                        assertResultsInstance.assertResults(resultMap)
                    } catch {
                        case a: AssertionError =>
                            a.printStackTrace()
                            hasAssertionErrors = true
                    }
                case _ =>
            }
        } catch {
            // If `InvocationTargetException` was thrown, an exception was thrown inside
            // perfSpecClass.getConstructor().newInstance(). Therefore we rethrow the cause.
            case e: InvocationTargetException => throw e.getCause
        }
    }

    /**
     * Execute a single measurement and capture the runtime and memory usage.
     *
     * @param measurementTuple The measurement to run as a tuple (measurementName, Measurement).
     * @return A [[MeasurementResult]] containing the considered execution times and memory usage.
     */
    private def runMeasurement(measurementTuple: (String, Measurement)): MeasurementResult = {
        val (name, measurement) = measurementTuple
        var consideredExecutionTimes = Seq.empty[Long]
        var memoryUsage: Long = -1

        logger.log(Level.INFO, s"${Utils.ANSI_GREEN}Measuring $name ${Utils.ANSI_RESET}")

        if (runRuntimeMeasurements && measurement.measureRuntime) {
            consideredExecutionTimes = measureRuntime(measurement)
        }

        if (runMemoryMeasurements && measurement.measureMemory) {
            memoryUsage = measureMemory(measurement)
        }

        MeasurementResult(
            name,
            consideredExecutionTimes,
            memoryUsage,
            measurement.runtimeWarningThreshold.getOrElse(
                RuntimeWarningThreshold(MeasurementExecutor.defaultRuntimeThresholdWarning)
            ),
            measurement.runtimeErrorThreshold.getOrElse(
                RuntimeErrorThreshold(MeasurementExecutor.defaultRuntimeThresholdError)
            ),
            measurement.memoryWarningThreshold.getOrElse(
                MemoryWarningThreshold(MeasurementExecutor.defaultMemoryThresholdWarning)
            ),
            measurement.memoryErrorThreshold.getOrElse(
                MemoryErrorThreshold(MeasurementExecutor.defaultMemoryThresholdError)
            )
        )
    }

    /**
     * Measure the runtime of a given function.
     *
     * @param measurement A measurement containing the function and thresholds.
     * @return A Seq of Nanoseconds containing the considered runtime in nanoseconds.
     */
    private def measureRuntime(measurement: Measurement): Seq[Long] = {
        var consideredExecutionTimes = Seq.empty[Long]

        var round = 1
        logger.log(Level.FINE, s"\r- Running round $round")
        time(2, 4, 3, measurement.f(), runGC = true) { (t, ts) =>
            consideredExecutionTimes = ts.map(_.timeSpan)
            round += 1
            logger.log(Level.FINE, s"\r- Running round $round")
        }
        logger.log(Level.FINE, s"\r- Running round $round...Done.\n")

        val avgRuntime = consideredExecutionTimes.sum / consideredExecutionTimes.length / 1000000000.0
        logger.log(Level.INFO,
            f"${Utils.ANSI_GREEN}- Average runtime: $avgRuntime%1.3f s${Utils.ANSI_RESET}")

        consideredExecutionTimes
    }

    /**
     * Measure the memory consumption of a given function f.
     *
     * @param measurement A measurement containing the function and thresholds.
     * @return The memory allocated in the function f in byte.
     */
    private def measureMemory(measurement: Measurement): Long = {
        val mm = new MemoryMeter()
        val o = measurement.f()

        // Run gc to remove any excess JVM overhead from the object
        gc() // org.opalj.util.gc

        val size = mm.measureDeep(o)

        logger.log(Level.INFO,
            s"${Utils.ANSI_GREEN}- Measurement uses ${size} byte (${Utils.asMB(size)}). ${Utils.ANSI_RESET}"
        )

        size
    }
}

/**
 * Factory for PerfSpecRunner instances.
 *
 * @author Andreas Muttscheller
 */
object PerfSpecRunner {
    def apply(
        perfSpecClass: Class[PerfSpec],
        runRuntimeMeasurements: Boolean = true,
        runMemoryMeasurements: Boolean = true
    ): PerfSpecRunner = new PerfSpecRunner(
        perfSpecClass,
        runRuntimeMeasurements,
        runMemoryMeasurements
    )
}
