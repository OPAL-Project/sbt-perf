/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.sbt.perf.spec

import org.opalj.sbt.perf.{
    MemoryErrorThreshold,
    MemoryWarningThreshold,
    RuntimeErrorThreshold,
    RuntimeWarningThreshold
}

/**
 * Class that contains information about the measurement
 *
 * @param name The name of the measurement. It must be unique to the class.
 * @param runtimeWarningThreshold A threshold when to write a warning on runtime measurements.
 * @param runtimeErrorThreshold A threshold when to throw an exception on runtime measurements.
 * @param memoryWarningThreshold A threshold when to write a warning on memory measurements.
 * @param memoryErrorThreshold A threshold when to throw an exception on memory measurements.
 * @param measureRuntime Measure the runtime of this measurement.
 * @param measureMemory Measure the memory of this measurement.
 * @param f A function to measure the runtime and memory consumption. If no value is returned,
 *          the memory won't be measured.
 */
private[perf] case class Measurement(
    name: String,
    f: () => Any,
    runtimeWarningThreshold: Option[RuntimeWarningThreshold] = None,
    runtimeErrorThreshold: Option[RuntimeErrorThreshold] = None,
    memoryWarningThreshold: Option[MemoryWarningThreshold] = None,
    memoryErrorThreshold: Option[MemoryErrorThreshold] = None,
    measureRuntime: Boolean = true,
    measureMemory: Boolean = true
)
