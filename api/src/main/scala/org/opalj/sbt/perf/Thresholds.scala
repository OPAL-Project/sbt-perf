/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.sbt.perf

import play.api.libs.json.{JsNumber, JsPath, Reads, Writes}

/**
 * Threshold when to print a warning on runtime comparison.
 *
 * @author Andreas Muttscheller
 */
case class RuntimeWarningThreshold(t: Double) extends AnyVal

/**
 * Threshold when to throw an error exception on runtime comparison.
 *
 * @author Andreas Muttscheller
 */
case class RuntimeErrorThreshold(t: Double) extends AnyVal

/**
 * Threshold when to print a warning on memory comparison.
 *
 * @author Andreas Muttscheller
 */
case class MemoryWarningThreshold(t: Double) extends AnyVal

/**
 * Threshold when to throw an error exception on memory comparison.
 *
 * @author Andreas Muttscheller
 */
case class MemoryErrorThreshold(t: Double) extends AnyVal

object RuntimeWarningThreshold {
    implicit val runtimeWarningThresholdWrites = new Writes[RuntimeWarningThreshold] {
        def writes(threshold: RuntimeWarningThreshold) = JsNumber.apply(threshold.t)
    }

    implicit val runtimeWarningThresholdReads: Reads[RuntimeWarningThreshold] =
        JsPath.read[Double].map(double ⇒ new RuntimeWarningThreshold(double))
}

object RuntimeErrorThreshold {
    implicit val runtimeErrorThresholdWrites = new Writes[RuntimeErrorThreshold] {
        def writes(threshold: RuntimeErrorThreshold) = JsNumber.apply(threshold.t)
    }

    implicit val runtimeErrorThresholdReads: Reads[RuntimeErrorThreshold] =
        JsPath.read[Double].map(double ⇒ new RuntimeErrorThreshold(double))
}

object MemoryWarningThreshold {
    implicit val memoryWarningThresholdWrites = new Writes[MemoryWarningThreshold] {
        def writes(threshold: MemoryWarningThreshold) = JsNumber.apply(threshold.t)
    }

    implicit val memoryWarningThresholdReads: Reads[MemoryWarningThreshold] =
        JsPath.read[Double].map(double ⇒ new MemoryWarningThreshold(double))
}

object MemoryErrorThreshold {
    implicit val memoryErrorThresholdWrites = new Writes[MemoryErrorThreshold] {
        def writes(threshold: MemoryErrorThreshold) = JsNumber.apply(threshold.t)
    }

    implicit val memoryErrorThresholdReads: Reads[MemoryErrorThreshold] =
        JsPath.read[Double].map(double ⇒ new MemoryErrorThreshold(double))
}