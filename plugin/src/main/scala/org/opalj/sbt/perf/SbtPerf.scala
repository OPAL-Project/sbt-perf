/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.sbt.perf

import java.lang.Double

import sbt.Keys._
import sbt._
import complete.DefaultParsers._
import sbt.complete.Parser

import scala.collection.mutable.ListBuffer

/**
 * sbt plugin to systematically measure the memory usage and runtime performance (experimental)
 * of code snippets/fragments and compare them with previous runs.
 *
 * @author Andreas Muttscheller
 */
object SbtPerf extends AutoPlugin {

    val sbtPerfVersion = getClass.getPackage.getImplementationVersion
    var PerfSpecDependency = "de.opal-project" % "perf-api_2.12" % sbtPerfVersion

    class MeasurementExecutorWrapper(val instance: AnyRef)

    object autoImport {
        val Perf = config("perf") extend Test
        val perf = inputKey[Unit]("Execute performance measurements on all available classes")
        val perfOnly = inputKey[Unit]("Execute performance measurements on given class")
        val runtimeThresholdWarning =
            settingKey[Double]("Threshold percentage runtime for a warning") in Perf
        val runtimeThresholdError =
            settingKey[Double]("Threshold percentage runtime for an error") in Perf
        val memoryThresholdWarning =
            settingKey[Double]("Threshold percentage memory usage for a warning") in Perf
        val memoryThresholdError =
            settingKey[Double]("Threshold percentage memory usage for an error") in Perf
        val cleanMeasurements = taskKey[Unit]("Clean all measurement results") in Perf
        val cleanMeasurement =
            inputKey[Unit]("Clean measurement results of the given class") in Perf
        val runRuntimeMeasurements =
            settingKey[Boolean]("Run runtime measurements") in Perf
        val runMemoryMeasurements =
            settingKey[Boolean]("Run memory measurements") in Perf
        val updateMeasurements =
            settingKey[Boolean]("Update measurements in .perf file if a measurement file exist") in
                Perf
        val forkedJvmOptions =
            settingKey[Seq[String]]("JVM options passed to the forked JVM") in Perf
    }

    import autoImport._

    override def trigger = allRequirements

    override def requires = plugins.JvmPlugin

    def makeAgentOptions(classpath: Classpath) : String = {
        val jammJar = classpath.map(_.data).filter(_.toString.contains("jamm")).head
        s"-javaagent:$jammJar"
    }

    lazy val PerfClassNameParser: Parser[String] = token(Space) ~>
        charClass(c => isScalaIDChar(c) || (c == '.'), "<perf class>").+.string

    override lazy val projectSettings =
        inConfig(Perf)(Defaults.testSettings ++ Defaults.testTasks ++ basePerfSettings) ++
            Seq(
                perf := perfTask.value,

                perfOnly := perfOnlyTask.evaluated,
                cleanMeasurements in Perf := cleanMeasurementsTask.value,
                cleanMeasurement in Perf := cleanMeasurementTask.evaluated,

                // OPAL must define the library dependency explicitly, because otherwise we run
                // into cyclic dependencies. Other projects can use it without explicit definition.
                libraryDependencies ++=(
                    if (organization.value == "de.opal-project")
                        Seq()
                    else
                        Seq(PerfSpecDependency)
                    )
            )

    lazy val basePerfSettings: Seq[Def.Setting[_]] = Seq(
        fullClasspath in Perf ++=
            ((fullClasspath in Test).value diff
                (fullClasspath in Perf).value),

        internalDependencyClasspath in Perf ++=
            ((internalDependencyClasspath in Test).value diff
                (internalDependencyClasspath in Perf).value),

        dependencyClasspath in Perf ++=
            ((dependencyClasspath in Test).value diff
                (dependencyClasspath in Perf).value),

        runtimeThresholdWarning in Perf := 0.2,
        runtimeThresholdError in Perf := 0.5,
        memoryThresholdWarning in Perf := 0.2,
        memoryThresholdError in Perf := 0.5,

        runRuntimeMeasurements in Perf := false,
        runMemoryMeasurements in Perf := true,
        updateMeasurements in Perf := false,

        forkedJvmOptions in Perf := Seq.empty[String]
    )

    /**
     * Execute performance measurement executor for all available classes
     */
    lazy val perfTask = Def.task {
        val args = setMeasurementOptions(
            runtimeThresholdWarning.value,
            runtimeThresholdError.value,
            memoryThresholdWarning.value,
            memoryThresholdError.value,
            runRuntimeMeasurements.value,
            runMemoryMeasurements.value,
            updateMeasurements.value
        )

        val classpath = (exportedProducts in Perf).value.files.mkString(",")

        forkMeasurementExecutor(
            args ++ Seq("--classpath", classpath),
            bootJars = (fullClasspath in Perf).value.files,
            jvmOptions = Seq(
                (dependencyClasspath in Perf).map(makeAgentOptions).value,
                "-Xmx6G"
            ) ++ (forkedJvmOptions in Perf).value
        )
    }

    /**
     * Run the performance measurement for one class only.
     */
    lazy val perfOnlyTask = Def.inputTask {
        val perfClass = PerfClassNameParser.parsed

        val args = setMeasurementOptions(
            runtimeThresholdWarning.value,
            runtimeThresholdError.value,
            memoryThresholdWarning.value,
            memoryThresholdError.value,
            runRuntimeMeasurements.value,
            runMemoryMeasurements.value,
            updateMeasurements.value
        )

        val classpath = (exportedProducts in Perf).value.files.mkString(",")
        forkMeasurementExecutor(
            args ++ Seq("--classpath", classpath) ++ Seq(perfClass),
            bootJars = (fullClasspath in Perf).value.files,
            jvmOptions = Seq(
                (dependencyClasspath in Perf).map(makeAgentOptions).value
            ) ++ (forkedJvmOptions in Perf).value
        )
    }

    /**
     * Clean the performance measurement file.
     */
    lazy val cleanMeasurementsTask = Def.task {
        forkMeasurementExecutor(
            Seq("--clean"),
            bootJars = (fullClasspath in Perf).value.files,
            jvmOptions = (forkedJvmOptions in Perf).value
        )
    }

    /**
     * Clean the performance measurement of the given class.
     */
    lazy val cleanMeasurementTask = Def.inputTask {
        val perfClass = PerfClassNameParser.parsed

        forkMeasurementExecutor(
            Seq("--cleanOnly", perfClass),
            bootJars = (fullClasspath in Perf).value.files,
            jvmOptions = (forkedJvmOptions in Perf).value
        )

    }

    /**
     * Fork the process and start a new VM running the `MeasurementExecutor` class. It also
     * adds the -javaagent for jamm to be able to run the memory measurements correctly.
     *
     * @note fork parameter in sbt works for run and test only (0.13.13).
     *
     * @param arguments A list of command line arguments passed to the main function of
     *                  the MeasurementExecutor.
     */
    def forkMeasurementExecutor(
        arguments: Seq[String],
        bootJars: Seq[java.io.File],
        jvmOptions: Seq[String] = Seq()
    ) = {
        val mainClass: String = "org.opalj.sbt.perf.Main"
        val forkOptions = ForkOptions()
            .withBootJars(bootJars.toVector)
            .withRunJVMOptions(jvmOptions.toVector)

        val process = Fork.java.fork(forkOptions, mainClass +: arguments)
        try {
            process.exitValue()
        } catch {
            case _: InterruptedException => process.destroy()
        }
    }

    /**
     * Create an argument Seq containing the thresholds and the switches for the main method
     * of the MeasurementsExecutor class.
     *
     * @param runtimeThresholdWarning     The warning threshold for runtime measurements.
     * @param runtimeThresholdError       The error threshold for runtime measurements.
     * @param memoryThresholdWarning      The warning threshold for memory measurements.
     * @param memoryThresholdError        The error threshold for memory usage measurements.
     * @param runRuntimeMeasurements      Sets if the runtime measurements shall be executed.
     * @param runMemoryMeasurements       Sets if the memory measurements shall be executed.
     * @param updateMeasurements          Update the measurements in .perf file.
     */
    private def setMeasurementOptions(
        runtimeThresholdWarning: Double,
        runtimeThresholdError: Double,
        memoryThresholdWarning: Double,
        memoryThresholdError: Double,
        runRuntimeMeasurements: Boolean,
        runMemoryMeasurements: Boolean,
        updateMeasurements: Boolean
    ): Seq[String] = {
        var arguments = ListBuffer(
            "--runtimeThresholdWarning", runtimeThresholdWarning.toString,
            "--runtimeThresholdError", runtimeThresholdError.toString,
            "--memoryThresholdWarning", memoryThresholdWarning.toString,
            "--memoryThresholdError", memoryThresholdError.toString
        )

        if (!runRuntimeMeasurements) {
            arguments += "--disableRuntimeMeasurements"
        }
        if (!runMemoryMeasurements) {
            arguments += "--disableMemoryMeasurements"
        }
        if (updateMeasurements) {
            arguments += "--updateMeasurements"
        }

        arguments
    }
}
