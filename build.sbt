import Dependencies._
import sys.process._

// Create jamm javaagent jvm option
def makeAgentOptions(classpath:Classpath) : String = {
    val jammJar = classpath.map(_.data).filter(_.toString.contains("jamm")).head
    s"-javaagent:$jammJar"
}

lazy val perf = (project in file(".")).
    aggregate(api, plugin).
    settings(
        skip in publish := true,
        scalastyle := {},
        scalastyle in Test := {}
    )

lazy val api = (project in file("api")).
    // We would like to: "disablePlugins(bintray.BintrayPlugin)." but it is not support...
    settings(Commons.settings: _*).
	settings(
        scalaVersion := "2.12.6",
        name := "perf-api",
        libraryDependencies ++= specDependencies,
        description := "evaluate the memory required to store an object (graph)",
        bintrayOrganization in bintray := None,
        publishMavenStyle := true,
        publishTo  := {
        	val nexus = "https://oss.sonatype.org/"
        	if (isSnapshot.value)
        		Some("snapshots" at nexus + "content/repositories/snapshots")
        	else
        		Some("releases"  at nexus + "service/local/staging/deploy/maven2")
        },
        publishArtifact in Test := false,
        pomExtra := (
          <scm>
            <url>git@bitbucket.org:OPAL-Project/sbt-perf.git</url>
            <connection>scm:git:git@bitbucket.org:OPAL-Project/sbt-perf.git</connection>
          </scm>
          <developers>
            <developer>
                <id>amuttsch</id>
                <name>Andreas Muttscheller</name>
            </developer>
            <developer>
                <id>eichberg</id>
                <name>Michael Eichberg</name>
                <url>http://www.michael-eichberg.de</url>
            </developer>
          </developers>
        ),

        fork in Test := true,
        javaOptions in Test += (dependencyClasspath in Test).map(makeAgentOptions).value,

        // Scalastyle options
        scalastyleConfigUrl in Test :=
            Some(url("https://bitbucket.org/delors/opal/raw/master/scalastyle-config.xml")),
        scalastyleConfigUrl in Compile :=
            Some(url("https://bitbucket.org/delors/opal/raw/master/scalastyle-config.xml")),
				(test in Test) := ((test in Test) dependsOn ((scalastyle in Test).toTask(""))).value,
				(compile in Compile) := ((compile in Compile) dependsOn ((scalastyle in Compile).toTask(""))).value
	)

lazy val plugin = (project in file("plugin")).
    disablePlugins(xerial.sbt.Sonatype).
    settings(Commons.settings: _*).
    settings(
        scalaVersion := "2.12.6",
        name := "sbt-perf",
        description := "plugin to run performance tests",
        bintrayOrganization in bintray := None,
        sbtPlugin := true,
        publishMavenStyle := false,
        bintrayRepository := "sbt-plugins",
        bintrayPackage := name.value,
        bintrayOrganization in bintray := None,
        // SCoverage settings
        // Workaround for highlighting issue, see https://github.com/scoverage/sbt-scoverage#highlighting
        coverageHighlighting := false,
        // Scalastyle options
        scalastyleConfigUrl in Compile :=
            Some(url("https://bitbucket.org/delors/opal/raw/master/scalastyle-config.xml")),
        scalastyleConfigUrl in Test :=
            Some(url("https://bitbucket.org/delors/opal/raw/master/scalastyle-config.xml")),
				(test in Test) := ((test in Test) dependsOn ((scalastyle in Test).toTask(""))).value,
				(compile in Compile) := ((compile in Compile) dependsOn ((scalastyle in Compile).toTask(""))).value
    )
